package client.rest;

import beans.models.Ticket;
import beans.rest.BookTicketRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@Service
public class RestClient {
    private static final MediaType APPLICATION_PDF = MediaType.valueOf("application/pdf");

    @Autowired
    private RestTemplate restTemplate;

    void call() {
        ticketPrice();
        bookTicketAsJson();
        bookTicketAsPdf();
        getTicketsForEventAsJson();
        getTicketsForEventAsPdf();
    }

    private void getTicketsForEventAsPdf() {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("event", "Joker Conf");
        urlVariables.put("auditorium", "Blue hall");
        urlVariables.put("dateTime", "2018-09-17 18:00");

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(APPLICATION_PDF));

        final HttpEntity<BookTicketRequest> entity = new HttpEntity<>(null, headers);

        final ResponseEntity<byte[]> ticketsForEvent = restTemplate.exchange(
                "getTicketsForEvent?event={event}&auditorium={auditorium}&dateTime={dateTime}",
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<byte[]>() {},
                urlVariables);

        if (ticketsForEvent.getStatusCode() == HttpStatus.OK) {
            System.out.printf("Tickets for event received as PDF with size %d bytes\n", ticketsForEvent.getBody().length);
        } else {
            System.out.printf("Unable to calculate ticket price. Server responded with HTTP code: %d", ticketsForEvent.getStatusCode().value());
        }
    }

    private void getTicketsForEventAsJson() {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("event", "Joker Conf");
        urlVariables.put("auditorium", "Blue hall");
        urlVariables.put("dateTime", "2018-09-17 18:00");

        final ResponseEntity<List<Ticket>> ticketsForEvent = restTemplate.exchange(
                "getTicketsForEvent?event={event}&auditorium={auditorium}&dateTime={dateTime}",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Ticket>>() {},
                urlVariables);

        if (ticketsForEvent.getStatusCode() == HttpStatus.OK) {
            System.out.println("Tickets for event:");
            for (Ticket ticket : ticketsForEvent.getBody()) {
                System.out.printf("\t%s bought by %s [Price: %.2f]\n", ticket.getEvent().getName(), ticket.getUser().getName(), ticket.getPrice());
            }
        } else {
            System.out.printf("Unable to calculate ticket price. Server responded with HTTP code: %d", ticketsForEvent.getStatusCode().value());
        }
    }

    private void ticketPrice() {
        Map<String, String> urlVariables = new HashMap<>();
        urlVariables.put("event", "Joker Conf");
        urlVariables.put("auditorium", "Blue hall");
        urlVariables.put("dateTime", "2018-09-17 18:00");
        urlVariables.put("seats", "11");
        urlVariables.put("userId", "1");

        final ResponseEntity<String> ticketPrice = restTemplate.getForEntity(
                "getTicketPrice?event={event}&auditorium={auditorium}&dateTime={dateTime}&seats={seats}&userId={userId}",
                String.class, urlVariables);

        if (ticketPrice.getStatusCode() == HttpStatus.OK) {
            System.out.printf("Ticket price is %.2f\n", Double.parseDouble(ticketPrice.getBody()));
        } else {
            System.out.printf("Unable to calculate ticket price. Server responded with HTTP code: %d", ticketPrice.getStatusCode().value());
        }
    }

    private void bookTicketAsJson() {
        final BookTicketRequest bookTicketRequest = new BookTicketRequest();
        bookTicketRequest.setUserId(4L);
        bookTicketRequest.setEventId(1);
        bookTicketRequest.setTime(LocalDateTime.of(2018, 9, 17, 18, 0, 0));
        bookTicketRequest.setSeats(Collections.singletonList(11));

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        final HttpEntity<BookTicketRequest> entity = new HttpEntity<>(bookTicketRequest, headers);
        final ResponseEntity<Ticket> ticket = restTemplate.postForEntity("bookTicket", entity, Ticket.class);

        if (ticket.getStatusCode() == HttpStatus.OK) {
            System.out.printf("Booked ticket: %s\n", ticket.getBody());
        } else {
            System.out.printf("Unable to book ticket. Server responded with HTTP code: %d", ticket.getStatusCode().value());
        }
    }

    private void bookTicketAsPdf() {
        final BookTicketRequest bookTicketRequest = new BookTicketRequest();
        bookTicketRequest.setUserId(4L);
        bookTicketRequest.setEventId(1);
        bookTicketRequest.setTime(LocalDateTime.of(2018, 9, 17, 18, 0, 0));
        bookTicketRequest.setSeats(Collections.singletonList(12));

        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(APPLICATION_PDF));

        final HttpEntity<BookTicketRequest> entity = new HttpEntity<>(bookTicketRequest, headers);
        final ResponseEntity<byte[]> ticket = restTemplate.postForEntity("bookTicket", entity, byte[].class);

        if (ticket.getStatusCode() == HttpStatus.OK) {
            System.out.printf("Booked ticket. Received PDF with size %d bytes\n", ticket.getBody().length);
        } else {
            System.out.printf("Unable to book ticket. Server responded with HTTP code: %d", ticket.getStatusCode().value());
        }
    }
}
