package client.rest;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@Configuration
public class RestClientApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(RestClientApp.class);
        final RestClient restClient = applicationContext.getBean(RestClient.class);
        restClient.call();
    }

    @Bean
    RestClient restClient() {
        return new RestClient();
    }

    @Bean
    RestTemplate restTemplate() {
        final DefaultUriTemplateHandler defaultUriTemplateHandler = new DefaultUriTemplateHandler();
        defaultUriTemplateHandler.setBaseUrl("http://localhost:8080/rest/booking/");

        final RestTemplate restTemplate = new RestTemplate();
        restTemplate.setUriTemplateHandler(defaultUriTemplateHandler);

        // PDF files will be received as byte arrays
        final ByteArrayHttpMessageConverter byteArrayHttpMessageConverter = new ByteArrayHttpMessageConverter();
        byteArrayHttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(new MediaType("application","pdf")));

        final List<HttpMessageConverter<?>> converters = new ArrayList<>(restTemplate.getMessageConverters());
        converters.add(byteArrayHttpMessageConverter);
        restTemplate.setMessageConverters(converters);

        return restTemplate;
    }
}
