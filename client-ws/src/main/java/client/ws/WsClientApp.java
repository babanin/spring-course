package client.ws;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@Configuration
public class WsClientApp {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(WsClientApp.class);
        final WsClient wsClient = applicationContext.getBean(WsClient.class);
        wsClient.call();
    }

    @Bean
    WsClient wsClient() {
        return new WsClient();
    }
}
