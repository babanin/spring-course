**All code located in forked repository: https://bitbucket.org/babanin/spring-course**

**Description to practical task #1**

_I tried to avoid significant changes to the template you've provided._

1. Dispatcher servlet declared in `WEB-INF/web.xml` and I used Jetty servlet container as Maven plugin (`mvn jetty:run`)
2. All booking operations defined in `beans/controller/BookingController.java` and views in `WEB-INF/view/ftl/booking`
3. All simple views are rendered using FreeMarker.
4. PDF reporting I've implemented as simple REST server parameterized by path variables `beans/controller/PdfController.java`: `${site}/pdf/byUser/${userId}/` and `${site}/pdf/byEvent/${eventId}/` 
5. Batch uploading `beans/controller/UploadController.java` with two examples in the root directory of project: `event.json` and `user.json`
6. Implemented as `@ControllerAdvice` and located in `beans/exception/GenericExceptionHandler.java`

**Description to practical task #2**
1. `DelegatingFilterProxy` was declared in web.xml and root context `WEB-INF/spring/applicationContext.xml` was created
2. `User` entity was updated with `roles` and `password` fields. Several test users were created in `import.sql`
3. Existing `UserService` now extends `UserDetailsService` and `UserServiceImpl` implements new method. New class `util.UserDetailsImpl` now implements `UserDetails` and used as container for user and authorities.
4. _Remember me_ functionality declared under `<http>` element in `WEB-INF/spring/applicationContext.xml` and utilizes table `PERSISTENT_LOGINS` created by Hibernate based on `PersistentLogin` entity and `persistentlogin.hbm.xml` mapping.
5. For testing purposes `NoOpPasswordEncoder` was used but without any difficulties it can be replaced with any other `PasswordEncoder` implementation.

**Description to practical task #3**
1. New entity was created `UserAccount` (`beans/models/UserAccount.java`) along with `UserAccountDAO` (`beans/daos/UserAccountDAO.java`) and it's implementation `UserAccountDAOImpl` (`beans/daos/db/UserAccountDAOImpl.java`). New field `ticketPrice` was added to `Event` entity.
2. Following methods were added to `UserAccountDAO`: 
    - replenishBalance 
    - withdrawBalance 
    - hasEnoughBalance
    
    On welcome page additional form for balance replenishing was added.

    In `BookingServiceImpl` (`beans/services/BookingServiceImpl.java`) service method `bookTicket` was enhanced to utilize `UserAccountDAO` methods for verifying that user has enough money to buy ticket and withdrawal.
3.  In `WEB-INF/spring/applicationContext.xml` `transactionManager` based on `HibernateTransactionManager`.
4. All methods in `BookingServiceImpl` were marked as `@Transactional`. 


**(SOAP Client) Description to practical task #4**

Not implemented yet


**(REST client) Description to practical task #5**
1. `beans/rest/BookingRestController.java` was implemented with same methods as `BookingService`
2. Two types of `HttpMessageConverter` were implemented: `beans/converter/TicketPdfHttpMessageConverter.java` for single ticket (used in `bookTicket`) and `beans/converter/TicketsPdfHttpMessageConverter.java` for multiple tickets (used in `getTicketsForEvent`)
3. `ContentNegotiationViewResolver` was declared in `dispatcher-servlet.xml` along with `ContentNegotiationManagerFactoryBean`
4. For testing purposes I created sub-project and client (`{PROJECT\client-rest\src\main\java\client\rest\RestClientApp.java`) with 5 different methods using `RestTemplate`.
5. Each booking REST service method was invoked with different `Accept` headers:
    - ticketPrice (GET/single value)
    - bookTicketAsJson (POST/JSON)
    - bookTicketAsPdf (POST/PDF)
    - getTicketsForEventAsJson (POST/JSON)
    - getTicketsForEventAsPdf (POST/PDF)
