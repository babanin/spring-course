<h2>Tickets for ${eventName} in ${time} at ${auditoriumName}</h2>

<table style="border-width: 1px; border-style: solid" border="1">
    <tr>
        <th>Seat</th>
        <th>Price</th>
        <th>User</th>
    </tr>

<#list ticketsForEvent as ticket>
<tr>
    <td>${ticket.seats}</td>
    <td>${ticket.price}</td>
    <td>${ticket.user.name}</td>
</tr>
</#list>
<table>


    <hr>

    <a href="/">Booking page</a>
