<h3>Replenish balance</h3>

<form action="/booking/replenishBalance" method="post" enctype="multipart/form-data">
    <label for="userId">User: </label>
    <select id="userId" name="userId">
    <#list users as user>
        <option value="${user.id}">${user.name} (${user.email})</option>
    </#list>
    </select>

    <br>

    <label for="value">Value: </label>
    <input type="text" name="value" id="value">

    <br>
    <br>

    <input type="submit" value="Replenish">
</form>

<h3>Ticket price</h3>
<form action="/booking/ticketPrice" method="post" enctype="multipart/form-data">
    <label for="eventName">Select name of event: </label>
    <select id="eventName" name="eventName">
    <#list events as event>
        <option value="${event.name}">${event.name}</option>
    </#list>
    </select>

    <br>

    <label for="auditoriumName">Select name of auditorium: </label>
    <select id="auditoriumName" name="auditoriumName">
    <#list auditoriums as auditorium>
        <option value="${auditorium.name}">${auditorium.name}</option>
    </#list>
    </select>

    <br>

    <label for="userId">User: </label>
    <select id="userId" name="userId">
    <#list users as user>
        <option value="${user.id}">${user.name} (${user.email})</option>
    </#list>
    </select>

    <br>

    <label for="time">Select time: </label>
    <select id="time" name="time">
    <#list timeStamps as timeStamp>
        <option value="${timeStamp}">${timeStamp}</option>
    </#list>
    </select>


    <br>

    <label for="seats">Seats (separated by comma): </label>
    <input type="text" name="seats" id="seats">

    <br>
    <br>

    <input type="submit" value="Calculate price">
</form>

<br>

<h3>Book ticket</h3>
<form action="/booking/bookTicket" method="post" enctype="multipart/form-data">
    <label for="eventId">Select name of event: </label>
    <select id="eventId" name="eventId">
    <#list events as event>
        <option value="${event.id}">${event.name}</option>
    </#list>
    </select>

    <br>

    <label for="auditoriumName">Select name of auditorium: </label>
    <select id="auditoriumName" name="auditoriumName">
    <#list auditoriums as auditorium>
        <option value="${auditorium.name}">${auditorium.name}</option>
    </#list>
    </select>

    <br>

    <label for="userId">User: </label>
    <select id="userId" name="userId">
    <#list users as user>
        <option value="${user.id}">${user.name}</option>
    </#list>
    </select>

    <br>

    <label for="time">Select time: </label>
    <select id="time" name="time">
    <#list timeStamps as timeStamp>
        <option value="${timeStamp}">${timeStamp}</option>
    </#list>
    </select>


    <br>

    <label for="seats">Seats (separated by comma): </label>
    <input type="text" name="seats" id="seats">

    <br>
    <br>

    <input type="submit" value="Book ticket">

</form>

<br>

<h3>All tickets for event</h3>
<form action="/booking/ticketsForEvent" method="post">
    <label for="eventName">Select name of event: </label>
    <select id="eventName" name="eventName">
    <#list events as event>
        <option value="${event.name}">${event.name}</option>
    </#list>
    </select>

    <br>

    <label for="auditoriumName">Select name of auditorium: </label>
    <select id="auditoriumName" name="auditoriumName">
    <#list auditoriums as auditorium>
        <option value="${auditorium.name}">${auditorium.name}</option>
    </#list>
    </select>

    <br>

    <label for="time">Select time: </label>
    <select id="time" name="time">
    <#list timeStamps as timeStamp>
        <option value="${timeStamp}">${timeStamp}</option>
    </#list>
    </select>

    <br>
    <br>

    <input type="submit" value="Find">
</form>

<hr>

<a href="/auth/logout">Logout</a>
