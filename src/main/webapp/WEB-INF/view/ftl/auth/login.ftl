<#if error = true>
    <div style="color: red">Invalid credentials</div>
</#if>

<#if logout = true>
    <div style="color: darkgreen">Successfully logged out</div>
</#if>

<form action="/auth/doLogin" method="post">
    <label for="username">Email: </label><input type="email" id="username" name="username"/><br>
    <label for="password">Password: </label><input type="password" id="password" name="password"/><br>
    <label for="rembemberMe">Remember me on this computer.</label><input type="checkbox" id="rembemberMe" name="_spring_security_remember_me"/><br>

    <input type="submit" value="Login">
</form>