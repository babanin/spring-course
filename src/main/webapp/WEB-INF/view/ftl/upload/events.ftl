<h2>Uploading finished</h2>
<h3>Events processed: </h3>
<ol>
<#list eventsProcessed as event>
    <li><b>${event.name}</b> (Rate: ${event.rate} / Price: ${event.basePrice})</li>
<#else>
    <h4>No users created</h4>
</#list>
</ol>
<hr>
<hr>
<a href="/../booking/">Booking</a> | <a href="/upload/">Upload</a>