<h2>Uploading finished</h2>
<h3>Users processed: </h3>

<ol>
<#list usersProcessed as user>
    <li><b>${user.name}</b> (${user.email})</li>
<#else>
    <h4>No users created</h4>
</#list>
</ol>
<hr>
<a href="/../booking/">Booking</a> | <a href="/upload/">Upload</a>