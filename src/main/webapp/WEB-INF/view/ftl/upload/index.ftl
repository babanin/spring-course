<h2>Users</h2>
<form action="/upload/users" enctype="multipart/form-data" method="post">
    <label for="file">Select JSON with user data:</label>
    <br>
    <input type="file" name="file"/>
    <br>
    <input type="submit" value="Upload">
</form>

<br>

<h2>Events</h2>
<form action="/upload/events" enctype="multipart/form-data" method="post">
    <label for="file">Select JSON with event data:</label>
    <br>
    <input type="file" name="file"/>
    <br>
    <input type="submit" value="Upload">
</form>
<hr>
<a href="/booking/">Booking</a>