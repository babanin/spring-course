package constants;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public final class DateTimeFormats {
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String DATE_TIME_FORMAT = "dd-MM-yyyy HH:mm";
}
