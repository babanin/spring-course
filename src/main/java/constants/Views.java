package constants;

public final class Views {
    public static final String EXCEPTION = "exception";
    public static final String NOT_FOUND = "notFound";

    public static final String UPLOAD_INDEX = "upload/index";
    public static final String UPLOAD_USERS_RESULT = "upload/users";
    public static final String UPLOAD_EVENTS_RESULT = "upload/events";

    public static final String BOOKING_INDEX = "booking/index";
    public static final String BOOKING_ALL_TICKETS_FOR_EVENT = "booking/ticketsForEvent";
    public static final String BOOKING_BOOK_TICKET = "booking/bookTicket";
    public static final String BOOKING_TICKET_PRICE = "booking/ticketPrice";
    public static final String BOOKING_REPLENISH_BALANCE = "booking/replenishBalance";

    public static final String AUTH_LOGIN = "auth/login";
}
