import beans.aspects.CounterAspect;
import beans.aspects.DiscountAspect;
import beans.aspects.LuckyWinnerAspect;
import beans.models.*;
import beans.services.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created with IntelliJ IDEA.
 * User: Dmytro_Babichev
 * Date: 2/4/2016
 * Time: 12:06 PM
 */
public class Main {

    public static void main(String[] args) throws IOException {
        User user1 = new User();
        user1.setName("Ivan");
        user1.setEmail("babanin@gmail.com");
        user1.setBirthday(LocalDate.now());

        User user2 = new User();
        user2.setName("Mark");
        user2.setEmail("mark.reinhold@oracle.com");
        user2.setBirthday(LocalDate.now());

        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);

        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        objectMapper.writeValue(new File("user.json"), users);

        Event event3 = new Event();
        event3.setName("Joker Conf");
        event3.setRate(Rate.MID);
        event3.setBasePrice(1000);
        event3.setDateTime(LocalDateTime.now());
        event3.setAuditorium(new Auditorium(0, "ExpoForum", 100, "11,22,33"));

        Event event4 = new Event();
        event4.setName("JPoint");
        event4.setRate(Rate.MID);
        event4.setBasePrice(2000);
        event4.setDateTime(LocalDateTime.now());
        event4.setAuditorium(new Auditorium(0, "Moscow", 100, "44,55,66"));

        List<Event> events = new ArrayList<>();
        events.add(event3);
        events.add(event4);

        objectMapper.writeValue(new File("event.json"), events);

        if(1==1)return;



        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");

        AuditoriumService auditoriumService = (AuditoriumService) ctx.getBean("auditoriumServiceImpl");
        BookingService bookingService = (BookingService) ctx.getBean("bookingServiceImpl");
        EventService eventService = (EventService) ctx.getBean("eventServiceImpl");
        UserService userService = (UserService) ctx.getBean("userServiceImpl");
        DiscountService discountService = (DiscountService) ctx.getBean("discountServiceImpl");

        String email = "dmitriy.vbabichev@gmail.com";
        String name = "Dmytro Babichev";
        String eventName = "The revenant";
        String auditoriumName = "Blue hall";
        Auditorium blueHall = auditoriumService.getByName(auditoriumName);
        Auditorium yellowHall = auditoriumService.getByName("Yellow hall");
        Auditorium redHall = auditoriumService.getByName("Red hall");
        LocalDateTime dateOfEvent = LocalDateTime.of(LocalDate.of(2016, 2, 5), LocalTime.of(15, 45, 0));

        userService.register(new User(email, name, LocalDate.now()));
        userService.register(new User("laory@yandex.ru", name, LocalDate.of(1992, 4, 29)));

        User userByEmail = userService.getUserByEmail(email);
        System.out.println("User with email: [" + email + "] is " + userByEmail);
        System.out.println();

        System.out.println("All users with name: [" + name + "] are: ");
        userService.getUsersByName(name).forEach(System.out:: println);
        System.out.println();

        Event event1 = eventService.create(
                new Event(eventName, Rate.HIGH, 60, LocalDateTime.of(LocalDate.of(2016, 2, 5), LocalTime.of(9, 0, 0)),
                          blueHall));
        System.out.println();
        System.out.println("Event by name: " + eventService.getByName(event1.getName()));
        System.out.println();
        eventService.create(new Event(eventName, Rate.HIGH, 60, dateOfEvent, blueHall));
        Event event2 = eventService.create(
                new Event(eventName, Rate.HIGH, 60, LocalDateTime.of(LocalDate.of(2016, 2, 5), LocalTime.of(21, 18, 0)),
                          blueHall));
        eventService.create(
                new Event(eventName, Rate.HIGH, 90, LocalDateTime.of(LocalDate.of(2016, 2, 5), LocalTime.of(21, 18, 0)),
                          redHall));
        Event event = new Event(eventName, Rate.HIGH, 150,
                                LocalDateTime.of(LocalDate.of(2016, 2, 5), LocalTime.of(21, 18, 0)), yellowHall);
        event = eventService.create(event);

        System.out.println("List of all events:");
        eventService.getAll().forEach(System.out:: println);
        System.out.println();

        System.out.println(
                "Discount for user: [" + email + "] for eventJson: [" + eventName + "] in auditorium: [" + auditoriumName +
                "] on date: [" + dateOfEvent + "] is [" +
                discountService.getDiscount(userByEmail, eventService.getEvent(eventName, blueHall, dateOfEvent))
                + "]");
        System.out.println();

        eventService.remove(event2);
        System.out.println("List of all events:");
        eventService.getAll().forEach(System.out:: println);
        System.out.println();

        List<Integer> seats = Arrays.asList(23, 24, 25, 26);
        double ticketPrice = bookingService.getTicketPrice(event.getName(), event.getAuditorium().getName(),
                                                           event.getDateTime(), seats, userByEmail);
        System.out.println("Price for eventJson: [" + event + "], seats: [" + seats + "], user: [" + userByEmail + "] = "
                           + ticketPrice);

        List<Integer> seats2 = Arrays.asList(27, 28, 29, 30);
        List<Integer> seats3 = Arrays.asList(2, 8, 9, 3);
        bookingService.bookTicket(userByEmail, new Ticket(event, LocalDateTime.now(), seats, userByEmail, ticketPrice));
        bookingService.bookTicket(userByEmail, new Ticket(event, LocalDateTime.now(), seats2, userByEmail,
                                                          bookingService.getTicketPrice(event.getName(),
                                                                                        event.getAuditorium().getName(),
                                                                                        event.getDateTime(), seats2,
                                                                                        userByEmail)));
        bookingService.bookTicket(userByEmail, new Ticket(event, LocalDateTime.now(), seats3, userByEmail,
                                                          bookingService.getTicketPrice(event.getName(),
                                                                                        event.getAuditorium().getName(),
                                                                                        event.getDateTime(), seats3,
                                                                                        userByEmail)));

        System.out.println();
        System.out.println("Tickets booked for eventJson: [" + event + "]");
        List<Ticket> ticketsForEvent = bookingService.getTicketsForEvent(event.getName(),
                                                                         event.getAuditorium().getName(),
                                                                         event.getDateTime());
        IntStream.range(0, ticketsForEvent.size()).forEach(
                i -> System.out.println("" + i + ") " + ticketsForEvent.get(i)));

        System.out.println();
        eventService.getByName("testName1");
        eventService.getByName("testName2");
        eventService.getByName("testName2");
        eventService.getByName("testName3");
        eventService.getByName(eventName);
        eventService.getEvent(event.getName(), event.getAuditorium(), event.getDateTime());

        bookingService.getTicketPrice(event.getName(), event.getAuditorium().getName(), event.getDateTime(), seats,
                                      userByEmail);
        bookingService.getTicketPrice(event.getName(), event.getAuditorium().getName(), event.getDateTime(), seats,
                                      userByEmail);

        System.out.println("CounterAspect.getAccessByNameStat() = " + CounterAspect.getAccessByNameStat());
        System.out.println("CounterAspect.getGetPriceByNameStat() = " + CounterAspect.getGetPriceByNameStat());
        System.out.println("CounterAspect.getBookTicketByNameStat() = " + CounterAspect.getBookTicketByNameStat());
        System.out.println();
        System.out.println("DiscountAspect.getDiscountStatistics() = " + DiscountAspect.getDiscountStatistics());
        System.out.println();
        System.out.println("LuckyWinnerAspect.getLuckyUsers() = " + LuckyWinnerAspect.getLuckyUsers());
    }
}
