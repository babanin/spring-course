package util.json;

import util.DateTimePatterns;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public class LocalDateXmlAdapter extends XmlAdapter<String, LocalDate> {
    private static DateTimeFormatter fmt = DateTimeFormatter.ofPattern(DateTimePatterns.DATE);

    public LocalDate unmarshal(String localDateString) throws Exception {
        return LocalDate.parse(localDateString, fmt);
    }

    public String marshal(LocalDate localDate) throws Exception {
        return fmt.format(localDate);
    }
}
