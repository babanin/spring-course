package util;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public final class DateTimePatterns {
    public static final String DATE = "yyyy-MM-dd";
    public static final String DATE_TIME = "yyyy-MM-dd HH:mm";
}
