package beans.daos;

import beans.models.User;
import beans.models.UserAccount;

public interface UserAccountDAO {
    double replenishBalance(User user, double value);

    double withdrawBalance(User user, double value);

    boolean hasEnoughBalance(User user, double value);

    UserAccount findByUser(User user);
}
