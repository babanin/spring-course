package beans.daos.db;

import beans.daos.AbstractDAO;
import beans.daos.UserAccountDAO;
import beans.models.User;
import beans.models.UserAccount;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository(value = "userAccountDAO")
public class UserAccountDAOImpl extends AbstractDAO implements UserAccountDAO {
    @Override
    public double replenishBalance(User user, double value) {
        return updateBalance(user, value);
    }

    @Override
    public double withdrawBalance(User user, double value) {
        return updateBalance(user, -value);
    }

    private double updateBalance(User user, double value) {
        UserAccount userAccount = findByUser(user);
        if (userAccount == null) {
            userAccount = new UserAccount();
            userAccount.setUser(user);

            getCurrentSession().persist(userAccount);
        }

        final double newBalance = userAccount.getBalance() + value;
        if (newBalance < 0) {
            throw new IllegalStateException(String.format(
                    "Balance can't be negative. Current: %.2f Value: %.2f", userAccount.getBalance(), Math.abs(value)));
        }

        userAccount.setBalance(newBalance);

        return newBalance;
    }

    @Override
    public boolean hasEnoughBalance(User user, double value) {
        final UserAccount userAccount = findByUser(user);
        return (userAccount != null) && (userAccount.getBalance() >= value);
    }

    @Override
    public UserAccount findByUser(User user) {
        return (UserAccount) createBlankCriteria(UserAccount.class).add(Restrictions.eq("user", user)).uniqueResult();
    }
}
