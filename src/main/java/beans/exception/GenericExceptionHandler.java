package beans.exception;

import constants.Views;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;

@ControllerAdvice
public class GenericExceptionHandler {
    private static String convertExceptionIntoString(Exception exception) {
        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);
        printWriter.close();

        final String result = stringWriter.toString();
        return result
                .replaceAll("\r\n", "<br>")
                .replaceAll("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
    }

    @ExceptionHandler(value = Exception.class)
    public ModelAndView handleException(HttpServletRequest request, HttpServletResponse response, Exception exception) {
        exception.printStackTrace();

        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        return new ModelAndView(Views.EXCEPTION)
                .addObject("exception", convertExceptionIntoString(exception))
                .addObject("exceptionMessage", exception.getMessage())
                .addObject("url", request.getRequestURL());
    }
}
