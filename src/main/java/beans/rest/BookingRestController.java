package beans.rest;

import beans.daos.EventDAO;
import beans.daos.UserDAO;
import beans.daos.db.EventDAOImpl;
import beans.models.Event;
import beans.models.Ticket;
import beans.models.User;
import beans.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import util.DateTimePatterns;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@RestController
@RequestMapping("/rest/booking")
public class BookingRestController {
    @Autowired
    @Qualifier("bookingServiceImpl")
    private BookingService bookingService;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private EventDAO eventDAO;

    @Transactional
    @RequestMapping(value = "/getTicketPrice", method = RequestMethod.GET)
    public double getTicketPrice(
            @RequestParam(required = false, name = "event") String event,
            @RequestParam(required = false, name = "auditorium") String auditorium,
            @RequestParam(required = false, name = "dateTime") @DateTimeFormat(pattern = DateTimePatterns.DATE_TIME) LocalDateTime dateTime,
            @RequestParam(required = false, name = "seats") String seats,
            @RequestParam(required = false, name = "userId") Long userId) {
        final List<Integer> seatList = Arrays.stream(seats.split(",")).map(Integer::parseInt).collect(Collectors.toList());

        return bookingService.getTicketPrice(event, auditorium, dateTime, seatList, userDAO.get(userId));
    }

    @Transactional
    @RequestMapping(value = "/bookTicket", method = RequestMethod.POST, produces = {"application/pdf", "application/json"})
    public @ResponseBody
    Ticket bookTicket(@RequestBody BookTicketRequest request) {
        final User user = userDAO.get(request.getUserId());
        final Event event = eventDAO.get(request.getEventId());

        final double ticketPrice = bookingService.getTicketPrice(
                event.getName(),
                event.getAuditorium().getName(),
                request.getTime(),
                request.getSeats(),
                user);

        final Ticket ticket = new Ticket(event, request.getTime(), request.getSeats(), user, ticketPrice);
        bookingService.bookTicket(user, ticket);

        return ticket;
    }

    @Transactional
    @RequestMapping(value = "/getTicketsForEvent", method = RequestMethod.GET, produces = {"application/pdf", "application/json"})
    public @ResponseBody
    List<Ticket> getTicketsForEvent(
            @RequestParam(required = false, name = "event") String event,
            @RequestParam(required = false, name = "auditorium") String auditorium,
            @RequestParam(required = false, name = "dateTime") @DateTimeFormat(pattern = DateTimePatterns.DATE_TIME) LocalDateTime dateTime) {
        return bookingService.getTicketsForEvent(event, auditorium, dateTime);
    }
}
