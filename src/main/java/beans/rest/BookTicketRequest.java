package beans.rest;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public class BookTicketRequest {
    private Long userId;
    private long eventId;

    private LocalDateTime time;
    private List<Integer> seats;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public List<Integer> getSeats() {
        return seats;
    }

    public void setSeats(List<Integer> seats) {
        this.seats = seats;
    }
}
