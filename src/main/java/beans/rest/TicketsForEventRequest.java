package beans.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import util.DateTimePatterns;

import java.time.LocalDateTime;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public class TicketsForEventRequest {
    private String event;

    private String auditorium;

    @JsonFormat(pattern = DateTimePatterns.DATE_TIME)
    private LocalDateTime date;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(String auditorium) {
        this.auditorium = auditorium;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
