package beans.rest;

import beans.models.User;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public class TicketPriceRequest {
    private String event;
    private String auditorium;
    private LocalDateTime dateTime;
    private List<Integer> seats;
    private User user;

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(String auditorium) {
        this.auditorium = auditorium;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public List<Integer> getSeats() {
        return seats;
    }

    public void setSeats(List<Integer> seats) {
        this.seats = seats;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
