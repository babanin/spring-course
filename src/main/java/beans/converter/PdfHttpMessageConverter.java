package beans.converter;

import beans.models.Ticket;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public abstract class PdfHttpMessageConverter<T> implements HttpMessageConverter<T> {
    static final MediaType APPLICATION_PDF = MediaType.valueOf("application/pdf");

    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
        return false;
    }

    @Override
    public List<MediaType> getSupportedMediaTypes() {
        return Collections.singletonList(APPLICATION_PDF);
    }

    @Override
    public T read(Class<? extends T> clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        return null;
    }

    @Override
    public void write(T value, MediaType contentType, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        outputMessage.getHeaders().set(HttpHeaders.CONTENT_TYPE, APPLICATION_PDF.toString());

        final Document document = new Document();

        final PdfWriter writer;
        try {
            writer = PdfWriter.getInstance(document, outputMessage.getBody());
        } catch (DocumentException e) {
            throw new IllegalStateException(String.format("Unable to create %s in %s for marshaling %s", PdfWriter.class.getSimpleName(),
                    PdfHttpMessageConverter.class.getSimpleName(), Ticket.class.getSimpleName()), e);
        }

        writer.setViewerPreferences(PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage);

        document.open();

        try {
            buildDocument(value, document);
        } catch (DocumentException e) {
            throw new IllegalStateException(String.format("Unable to generate a document for ticket: %s", value.toString()), e);
        }

        document.close();
    }

    protected abstract void buildDocument(T value, Document document) throws DocumentException;
}
