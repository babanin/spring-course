package beans.converter;

import beans.models.Ticket;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Table;
import org.springframework.http.MediaType;

import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public class TicketsPdfHttpMessageConverter extends PdfHttpMessageConverter<List<Ticket>> {
    @Override
    protected void buildDocument(List<Ticket> tickets, Document document) throws DocumentException {
        final DecimalFormat decimalFormat = new DecimalFormat("#######.#");
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        final Table table = new Table(3);
        table.addCell("Event");
        table.addCell("Date");
        table.addCell("Price");

        for (Ticket ticket : tickets) {
            table.addCell(ticket.getEvent().getName());
            table.addCell(dateTimeFormatter.format(ticket.getDateTime()));
            table.addCell(decimalFormat.format(ticket.getPrice()));
        }

        document.add(table);
    }

    @Override
    public boolean canWrite(Class<?> clazz, MediaType mediaType) {
        return List.class.isAssignableFrom(clazz) && APPLICATION_PDF.isCompatibleWith(mediaType);
    }
}
