package beans.view;

import beans.models.Ticket;
import com.lowagie.text.Document;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.DecimalFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public class TicketsPdfView extends AbstractPdfView {
    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer, HttpServletRequest request, HttpServletResponse response) throws Exception {
        final DecimalFormat decimalFormat = new DecimalFormat("#######.#");
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        final List<Ticket> tickets = (List<Ticket>) model.get("tickets");

        final Table table = new Table(3);
        table.addCell("Event");
        table.addCell("Date");
        table.addCell("Price");

        for (Ticket ticket : tickets) {
            table.addCell(ticket.getEvent().getName());
            table.addCell(dateTimeFormatter.format(ticket.getDateTime()));
            table.addCell(decimalFormat.format(ticket.getPrice()));
        }

        document.add(table);
    }
}
