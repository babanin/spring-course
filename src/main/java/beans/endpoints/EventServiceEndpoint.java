package beans.endpoints;

import beans.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.server.endpoint.annotation.Endpoint;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@Endpoint
public class EventServiceEndpoint {
    @Autowired
    @Qualifier("eventServiceImpl")
    private EventService eventService;


}
