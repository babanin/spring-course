package beans.controller;

import beans.daos.BookingDAO;
import beans.daos.EventDAO;
import beans.daos.UserDAO;
import beans.models.Event;
import beans.models.Ticket;
import beans.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static constants.Views.NOT_FOUND;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@Controller
@Transactional
@RequestMapping(value = "/pdf", headers = "Accept=application/pdf")
public class PdfController {
    @Autowired
    private EventDAO eventDAO;

    @Autowired
    private BookingDAO bookingDAO;

    @Autowired
    private UserDAO userDAO;

    @RequestMapping("/byEvent/{eventId}/")
    public ModelAndView ticketsByEvent(@PathVariable int eventId, HttpServletResponse response) {
        final Event event = eventDAO.get(eventId);
        if (event == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return new ModelAndView(NOT_FOUND)
                    .addObject("type", "Event")
                    .addObject("id", eventId);
        }

        final List<Ticket> tickets = bookingDAO.getTickets(event);
        return new ModelAndView("ticketsPdfView", "tickets", tickets);
    }

    @RequestMapping("/byUser/{userId}/")
    public ModelAndView ticketsByUser(@PathVariable int userId, HttpServletResponse response) {
        final User user = userDAO.get(userId);
        if (user == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return new ModelAndView(NOT_FOUND)
                    .addObject("type", "User")
                    .addObject("id", userId);
        }

        final List<Ticket> tickets = bookingDAO.getTickets(user);

        return new ModelAndView("ticketsPdfView", "tickets", tickets);
    }
}
