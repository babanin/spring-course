package beans.controller;

import beans.models.Event;
import beans.models.User;
import beans.services.UploadService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import constants.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.List;

import static constants.Views.UPLOAD_INDEX;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@Controller
@RequestMapping("/upload")
public class UploadController {
    @Autowired
    private UploadService uploadService;

    @RequestMapping
    public String index() {
        return UPLOAD_INDEX;
    }

    @RequestMapping(value = "/events", method = RequestMethod.POST)
    public ModelAndView events(@RequestParam("file") MultipartFile file) throws IOException {
        final ObjectMapper objectMapper = createObjectMapper();
        List<Event> events =  objectMapper.readValue(file.getInputStream(), new TypeReference<List<Event>>(){});
        final List<Event> eventsProcessed = uploadService.processEvents(events);
        return new ModelAndView(Views.UPLOAD_EVENTS_RESULT, "eventsProcessed", eventsProcessed);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public ModelAndView users(@RequestParam("file") MultipartFile file) throws IOException {
        final ObjectMapper objectMapper = createObjectMapper();
        List<User> users =  objectMapper.readValue(file.getInputStream(), new TypeReference<List<User>>(){});
        final List<User> usersProcessed = uploadService.processUsers(users);
        return new ModelAndView(Views.UPLOAD_USERS_RESULT, "usersProcessed", usersProcessed);
    }

    private static ObjectMapper createObjectMapper() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        return objectMapper;
    }
}
