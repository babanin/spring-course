package beans.controller;

import constants.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@Controller
@RequestMapping("/auth")
public class AuthController {
    @RequestMapping("/login")
    public ModelAndView login(
            @RequestParam(required = false) boolean error,
            @RequestParam(required = false) boolean logout) {
        return new ModelAndView(Views.AUTH_LOGIN)
                .addObject("error", error)
                .addObject("logout", logout);
    }
}
