package beans.controller;

import beans.daos.AuditoriumDAO;
import beans.daos.EventDAO;
import beans.daos.UserAccountDAO;
import beans.daos.UserDAO;
import beans.models.Auditorium;
import beans.models.Event;
import beans.models.Ticket;
import beans.models.User;
import beans.services.BookingService;
import constants.DateTimeFormats;
import constants.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class BookingController {
    @Autowired
    @Qualifier("bookingServiceImpl")
    private BookingService bookingService;

    @Autowired
    private EventDAO eventDAO;

    @Autowired
    private AuditoriumDAO auditoriumDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserAccountDAO userAccountDAO;

    @Transactional
    @RequestMapping("/")
    public ModelAndView index() {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DateTimeFormats.DATE_TIME_FORMAT);
        final List<User> users = userDAO.getAll();
        final List<Event> events = eventDAO.getAll();
        final List<Auditorium> auditoriums = auditoriumDAO.getAll();
        final List<String> timeStamps = events.stream().map(Event::getDateTime).map(dateTimeFormatter::format).collect(Collectors.toList());

        return new ModelAndView(Views.BOOKING_INDEX)
                .addObject("events", events)
                .addObject("users", users)
                .addObject("timeStamps", timeStamps)
                .addObject("auditoriums", auditoriums);
    }

    @Transactional
    @RequestMapping(value = "/booking/ticketsForEvent", method = RequestMethod.POST)
    public ModelAndView ticketsForEvent(@RequestParam(value = "eventName", required = false) String eventName,
                                        @RequestParam(value = "auditoriumName", required = false) String auditoriumName,
                                        @RequestParam(value = "time", required = false) @DateTimeFormat(pattern = DateTimeFormats.DATE_TIME_FORMAT) LocalDateTime time
    ) {
        final List<Ticket> ticketsForEvent = bookingService.getTicketsForEvent(eventName, auditoriumName, time);
        return new ModelAndView(Views.BOOKING_ALL_TICKETS_FOR_EVENT)
                .addObject("eventName", eventName)
                .addObject("auditoriumName", auditoriumName)
                .addObject("ticketsForEvent", ticketsForEvent)
                .addObject("time", time.format(DateTimeFormatter.ofPattern(DateTimeFormats.DATE_TIME_FORMAT)));
    }

    @Transactional
    @RequestMapping(value = "/booking/ticketPrice", method = RequestMethod.POST)
    public ModelAndView ticketPrice(@RequestParam(value = "eventName", required = false) String eventName,
                                    @RequestParam(value = "auditoriumName", required = false) String auditoriumName,
                                    @RequestParam(value = "userId", required = false) Long userId,
                                    @RequestParam(value = "time", required = false) @DateTimeFormat(pattern = DateTimeFormats.DATE_TIME_FORMAT) LocalDateTime time,
                                    @RequestParam(value = "seats", required = false) String seats
    ) {
        double ticketPrice = bookingService.getTicketPrice(
                eventName,
                auditoriumName,
                time,
                Arrays.stream(seats.split(",")).map(Integer::parseInt).collect(Collectors.toList()),
                userDAO.get(userId));
        return new ModelAndView(Views.BOOKING_TICKET_PRICE)
                .addObject("eventName", eventName)
                .addObject("auditoriumName", auditoriumName)
                .addObject("ticketPrice", ticketPrice);
    }

    @Transactional
    @RequestMapping("/booking/bookTicket")
    public ModelAndView bookTicket(
            @RequestParam(value = "userId", required = false) Long userId,
            @RequestParam(value = "eventId", required = false) long eventId,
            @RequestParam(value = "time", required = false) @DateTimeFormat(pattern = DateTimeFormats.DATE_TIME_FORMAT) LocalDateTime time,
            @RequestParam(value = "seats", required = false) String seats
    ) {
        final User user = userDAO.get(userId);
        final Event event = eventDAO.get(eventId);
        final List<Integer> listOfSeats = Arrays.stream(seats.split(",")).map(Integer::parseInt).collect(Collectors.toList());

        final double ticketPrice = bookingService.getTicketPrice(
                event.getName(),
                event.getAuditorium().getName(),
                time,
                listOfSeats,
                user);

        final Ticket ticket = new Ticket(event, time, listOfSeats, user, ticketPrice);
        bookingService.bookTicket(user, ticket);
        return new ModelAndView(Views.BOOKING_BOOK_TICKET)
                .addObject("ticket", ticket);
    }

    @Transactional
    @RequestMapping("/booking/replenishBalance")
    public ModelAndView replenishBalance(
            @RequestParam(value = "userId", required = false) Long userId,
            @RequestParam(value = "value", required = false) double value
    ) {
        final User user = userDAO.get(userId);
        final double balance = userAccountDAO.replenishBalance(user, value);

        return new ModelAndView(Views.BOOKING_REPLENISH_BALANCE)
                .addObject("user", user)
                .addObject("balance", balance);
    }
}
