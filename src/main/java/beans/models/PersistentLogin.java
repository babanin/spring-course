package beans.models;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
public class PersistentLogin {
    private String userName;
    private String series;
    private String token;
    private LocalDateTime lastUsed;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(LocalDateTime lastUsed) {
        this.lastUsed = lastUsed;
    }
}
