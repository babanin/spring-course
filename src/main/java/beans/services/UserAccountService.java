package beans.services;

public interface UserAccountService {
    double replenish(String email, double value);

    double withdraw(String email, double value);
}
