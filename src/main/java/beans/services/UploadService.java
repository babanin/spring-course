package beans.services;

import beans.daos.AuditoriumDAO;
import beans.daos.EventDAO;
import beans.daos.UserDAO;
import beans.models.Auditorium;
import beans.models.Event;
import beans.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ivan Babanin (babanin@gmail.com)
 */
@Service
@Transactional
public class UploadService {

    @Autowired
    private EventDAO eventDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private AuditoriumDAO auditoriumDAO;

    public List<Event> processEvents(List<Event> events) {
        List<Event> result = new ArrayList<>();
        for (Event jsonEvent : events) {
            final Auditorium jsonAuditorium = jsonEvent.getAuditorium();
            final Auditorium dbAuditorium = auditoriumDAO.getByName(jsonAuditorium.getName());
            jsonEvent.setAuditorium(dbAuditorium);
            result.add(eventDAO.create(jsonEvent));
        }

        return result;
    }

    public List<User> processUsers(List<User> users) {
        List<User> result = new ArrayList<>();
        for (User jsonUser : users) {
            result.add(userDAO.create(jsonUser));
        }

        return result;
    }
}
