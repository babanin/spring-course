package beans.services;

import beans.daos.UserAccountDAO;
import beans.daos.UserDAO;
import beans.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserAccountServiceImpl implements UserAccountService {
    private final UserAccountDAO userAccountDao;
    private final UserDAO userDAO;

    @Autowired
    public UserAccountServiceImpl(UserAccountDAO userAccountDao, UserDAO userDAO) {
        this.userAccountDao = userAccountDao;
        this.userDAO = userDAO;
    }

    @Override
    public double replenish(String email, double value) {
        final User user = userDAO.getByEmail(email);
        return userAccountDao.replenishBalance(user, value);
    }

    @Override
    public double withdraw(String email, double value) {
        final User user = userDAO.getByEmail(email);
        return userAccountDao.withdrawBalance(user, value);
    }
}
